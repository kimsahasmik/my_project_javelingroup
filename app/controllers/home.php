<?php
class Home extends Controller
{
	public function index($name = '')
	{
		$user = $this->model('home');
		$user->name = $name;
		
		$this->view('home/index', ['name'=>$user->name]);
		
	}		
}

?>