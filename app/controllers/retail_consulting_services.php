<?php
class Retail_consulting_services extends Controller
{
	public function index($name = '')
	{
		$user = $this->model('retail_consulting_services');
		$user->name = $name;
		
		$this->view('retail_consulting_services/index', ['name'=>$user->name]);
		
	}		
}

?>