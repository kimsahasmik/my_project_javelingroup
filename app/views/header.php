<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
   	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Javelin Group</title>

	<link href="../css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="../css/reset.css" rel="stylesheet" type="text/css">
    <link href="../css/style.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
</head>
<body>
	<header>
		<div class="row" id="search1">
			<div class="container-fluid container_close">
				<div class="container">
					<div class="colse_search"><i class="fa fa-close" aria-hidden="true"></i></div>
				</div>
			</div>
			<div class="container-fluid container_form" id="search2">
				<div class="container">
					<div class="search_form">
						<form>
							<input type="search" name="" class="search_name" placeholder="Search this website" autofocus>
							<button class="search_submit">
								<i class="fa fa-search"></i>
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-fixed-top main-header" role="navigation">
			<div class="container">
				<div class="row">
    				<ul class="navbar-header">
                        <div class="mobile_menu">
    					<li class="navbar-toggle p-0" data-toggle="collapse" data-target="#myNavbar">
    						<i class="fa fa-bars"></i>
    					</li>
    					<li class="navbar-toggle p-0" data-toggle="collapse" data-target="#lang">
    						<ul>
    							<li><a href="#"><img src="../images/en.png" alt=""></a></li>
    							<li class="collapse navbar-collapse" id="lang"><a href="#"><img src="../images/fr.png" alt="fr flag"></a></li>
    						</ul>
    					</li>
    					<li class="navbar-toggle p-0" data-toggle="collapse" data-target="#search1">
    						<i class="fa fa-search search"></i>
    					</li></div>
                        <a class="navbar-brand logo pl-15px" href="./"></a>
    				</ul><!--end navbar-header-->
        
    			<div class="navbar-right nav_bar">
    				<nav class="collapse navbar-collapse navbar-right" id="search">
    					<ul class="nav navbar-nav">
    						<li class="search"><a href="#"><span class="glyphicon glyphicon-search"></span></a></li>
    					</ul>
    				</nav>
    				<nav class="collapse navbar-collapse navbar-right lang">
    					<ul class="nav navbar-nav navbar-light bg-faded">
    						<li><a href="#"><img src="../images/en.png" alt="">English</a></li>
    						<li><a href="#"><img src="../images/fr.png" alt="">Français</a></li>
    					</ul>
    				</nav>

    				<nav class="collapse navbar-collapse" id="myNavbar">
    					<ul class="nav navbar-nav navbar-light bg-faded navbar-left navbar1 navbar_show">
							<li><a href="./">Home</a></li>
							<li><a href="#">Careers</a></li>
						</ul>
				  		<ul class="nav navbar-nav navbar-light bg-faded navbar2">
				  	  		<li class="dropdown">
				  	  			<a class="dropdown-toggle dis" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="#">About
				  	  				<i class="fa fa-angle-down transition"></i>
				  	  			</a>
				  	  				
				  	  			<ul class="dropdown-menu">
				  	  		  	  <li><a href="./about">Company</a></li>
				  	  		  	  <li><a href="#">Management</a></li>
				  	  		  	  <li><a href="#">Events</a></li>
				  	  		  	  <li><a href="#">Press Releases</a></li>
				  	  		  	  <li><a href="#">In the News</a></li> 
				  	  		  	</ul>
				  	  		</li>
							<li class="dropdown">
								<a class="dropdown-toggle dis" data-toggle="dropdown" role="button" href="#" aria-haspopup="true" aria-expanded="false">Services
								<i class="fa fa-angle-down transition"></i></a>
								<ul class="dropdown-menu">
				  	  		  	  <li><a href="./retail_consulting_services">Overview</a></li>
				  	  		  	  <li><a href="#">Retail Strategy</a></li>
				  	  		  	  <li><a href="#">Omni-Channel Retail</a></li>
				  	  		  	  <li><a href="#">Operations</a></li> 
				  	  		  	  <li><a href="#">Locations & Analytics</a></li> 
				  	  		  	  <li><a href="#">Technology Consulting</a></li> 
				  	  		  	  <li><a href="#">Due Diligence</a></li> 
				  	  		  	  <li><a href="#">Systems Integration</a></li>  
				  	  		  	</ul>
							</li>
							<li class="dropdown">
								<a class="dropdown-toggle dis" data-toggle="dropdown" href="#">Clients
								<i class="fa fa-angle-down transition"></i></a>
								<ul class="dropdown-menu">
				  	  		  	  <li><a href="#">Client List</a></li>
				  	  		  	  <li><a href="#">Case Studies</a></li>
				  	  		  	</ul>
							</li>
							<li class="dropdown">
								<a class="dropdown-toggle dis" data-toggle="dropdown" href="#">Insights
								<i class="fa fa-angle-down transition"></i></a>
								<ul class="dropdown-menu">
				  	  		  	  <li><a href="#">White Papers</a></li>
				  	  		  	  <li><a href="#">Features</a></li>
				  	  		  	</ul>
							</li>
							<li class="dropdown">
								<a class="dropdown-toggle dis" data-toggle="dropdown" href="#">Blog
								<i class="fa fa-angle-down transition"></i></a>
								<ul class="dropdown-menu">
				  	  		  	  <li><a href="#">The Retail Blog</a></li>
				  	  		  	</ul>
							</li>
							<li class="dropdown">
								<a class="dropdown-toggle dis" data-toggle="dropdown" href="#">Contact
								<i class="fa fa-angle-down transition"></i></a>
								<ul class="dropdown-menu">
				  	  		  	  <li><a href="#">United Kingdom</a></li>
				  	  		  	  <li><a href="#">France</a></li>
				  	  		  	  <li><a href="#">Bulgaria</a></li> 
				  	  		  	</ul>
							</li>
				  		</ul>
    				</nav><!--end mynavbar-->

    			</div><!--end navbar-right-->
    			</div><!--end row-->
  			</div><!--end container-->
  		</nav><!--navbar-default-->
  		<a href="">
  		<div class="fixed-tab js--back-to-top">
  			<i class="fa  fa-angle-up"></i>
  		</div>
  	</a>
	</header><!--end header-->