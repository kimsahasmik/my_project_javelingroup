<section>
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="../images/1_Strategic_Retail_alt_1920-x-565.jpg" alt="Chania" width="100%">
        <div class="carousel-caption">
          <h1>STRATEGIC RETAIL TRANSFORMATION</h1>
          <p>Retail strategy consulting and digital transformation services.</p>
          <a class="more" href="">Find out more</a>
        </div>
      </div>

      <div class="item">
        <img src="../images/homepage-slide-2-large.jpg" alt="Chania" width="100%">
        <div class="carousel-caption">
          <h1>DIGITAL TRANSFORMATION</h1>
          <p>Helping major retailers and brands address the challenges of the digital age.</p>
          <a class="more" href="">Find out more</a>
        </div>
      </div>
    
      <div class="item">
        <img src="../images/homepage-slide-3-large.jpg" alt="Flower" width="100%">
        <div class="carousel-caption">
          <h1>VERTICAL RETAIL SPECIALISTS</h1>
          <p>Strategy + Operations + Technology + Digital + Stores + Data.</p>
          <a class="more" href="">Find out more</a>
        </div>
      </div>

      <div class="item">
        <img src="../images/homepage-slide-4-large.jpg" alt="Flower" width="100%">
        <div class="carousel-caption">
          <h1>RETAILERS AND BRANDS WORLDWIDE</h1>
          <p>Working with leaders in every major retail market in the world.</p>
          <a class="more" href="">Find out more</a>
        </div>
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control prev" href="#myCarousel" role="button" data-slide="prev">
      <img src="../images/prev.png">
    </a>
    <a class="right carousel-control next" href="#myCarousel" role="button" data-slide="next">
      <img src="../images/next.png">
    </a>
  </div>

  <div class="container-fluid">
  	<div class="container">
  		<div id="myCarousel2" class="carousel slide" data-ride="carousel">
    		<ol class="carousel-indicators">
    		  <li data-target="#myCarousel2" data-slide-to="0" class="active"></li>
    		  <li data-target="#myCarousel2" data-slide-to="1"></li>
    		  <li data-target="#myCarousel2" data-slide-to="2"></li>
    		  <li data-target="#myCarousel2" data-slide-to="3"></li>
    		  <li data-target="#myCarousel2" data-slide-to="4"></li>
    		  <li data-target="#myCarousel2" data-slide-to="5"></li>
    		  <li data-target="#myCarousel2" data-slide-to="6"></li>
    		  <li data-target="#myCarousel2" data-slide-to="7"></li>
    		  <li data-target="#myCarousel2" data-slide-to="8"></li>
    		  <li data-target="#myCarousel2" data-slide-to="9"></li>
    		  <li data-target="#myCarousel2" data-slide-to="10"></li>
    		  <li data-target="#myCarousel2" data-slide-to="11"></li>
    		  <li data-target="#myCarousel2" data-slide-to="12"></li>
    		</ol>

    		<div class="carousel-inner" role="listbox">
    		  	<div class="item active">
    		  		<a href="">
    		    		<img src="../images/future_retail_homepage.png" alt="Chania" width="137" height="137">
    		    		<div class="container">
    		    			<div class="carousel-caption">
    		    				<h2>Blog</h2>
    		    				<h4>What’s in store for the next 10 years of retail?</h4>
    		    				<p>How will technology, innovation and the customer shape the future of the retail industry over the next 10 years and how can retailers embrace these developments?</p>
    		    			</div>
    		    		</div>
    		    	</a>
    			</div>
		
    		 	<div class="item">
    		 		<a href="">
    		 	  		<img src="../images/venuescore-2017.png" alt="Chania" width="137" height="137">
    		 	  		<div class="carousel-caption">
    		 	  			<h2>News</h2>
    		 	  			<h4>New Executive Summary: VENUESCORE 2017.</h4>
    		 	  			<p>VENUESCORE is an annual survey, which ranks the UK's top 3,500+ retail venues and provides retailers, developers, owners and brands with an up-to-date straightforward tool for understanding shopping venues.</p>
    		 	  		</div>
    		 	  	</a>
    		 	</div>
    		
    		  	<div class="item">
    		  		<a href="">
    		  	  		<img src="../images/omni-channel_retail_environment.png" alt="Flower" width="137" height="137">
    		  	  		<div class="carousel-caption">
    		  	  			<h2>Blog</h2>
    		 	  			<h4>Omni-channel retail – environmental risk or opportunity?</h4>
    		 	  			<p>With omni-channel fast becoming the dominant retail model resulting in complex supply chains, what are the environmental challenges and opportunities for retailers aiming for a carbon-efficient future?</p>
    		 	  	    </div>
    		 	  	</a>
    		  	</div>
			
    		  	<div class="item">
    		  		<a href="">
    		  	  	<img src="../images/benchmark_logistics_homepage.png" alt="Flower" width="137" height="137">
    		  	  		<div class="carousel-caption">
    		  	  			<h2>Blog</h2>
    		 	  			<h4>Do you need to benchmark your retail and ecommerce logistics costs?</h4>
    		 	  			<p>With logistics costs typically higher for ecommerce than store retail, and sales increasingly moving online, there is a growing focus on cost to serve. For retailers, a logistics costs benchmarking exercise can prove very insightful.</p>
    		  	  		</div>
    		  	  	</a>
    		  	</div>
    		  
    		  	<div class="item">
    		  		<a href="">
    		    		<img src="../images/airport_retailing_blog_homepage-1.png" alt="Flower" width="137" height="137">
    		    		<div class="carousel-caption">
    		    			<h2>Blog</h2>
    		 	  			<h4>Is USA airport retailing about to be revolutionised?</h4>
    		 	  			<p>Air travel is big business. With global passenger numbers predicted to double by 2019, and exceed 23bn by 2040, what can USA airports learn from the rest of the world?</p>
    		    		</div>
    		    	</a>
    		  	</div>
    		  
    		  	<div class="item">
    		  		<a href="">
    		    		<img src="../images/online_grocery_blog-1.png" alt="Flower" width="137" height="137">
    		    		<div class="carousel-caption">
    		    			<h2>Blog</h2>
    		 	  			<h4>Can online grocery be profitable?</h4>
    		 	  			<p>What are the nine levers grocery retailers need to consider in order to deliver a profitable online grocery offer?</p>
    		    		</div>
    		    	</a>
    		  	</div>
    		  
    		  	<div class="item">
    		  		<a href="">
    		    		<img src="../images/international_ecommerce_warehousing.png" alt="Flower" width="137" height="137">
    		    		<div class="carousel-caption">
    		    			<h2>Blog</h2>
    		 	  			<h4>International ecommerce – do you need a warehouse overseas?</h4>
    		 	  			<p>How can UK retailers with significant overseas sales structure their warehouse operations to successfully manage international ecommerce?</p>
    		    		</div>
    		    	</a>
    		  	</div>
    		  
    		  	<div class="item">
    		  		<a href="">
    		    		<img src="../images/home_delivery-1.png" alt="Flower" width="137" height="137">
    		    		<div class="carousel-caption">
    		    			<h2>Blog</h2>
    		 	  			<h4>The future of home delivery - 10 likely developments.</h4>
    		 	  			<p>Home delivery has become a strategic differentiator for retailers, with customers now offered a large number of options, but what will the future look like?</p>
    		    		</div>
    		    	</a>
    		  	</div>
    		  
    		  	<div class="item">
    		  		<a href="">
    		  	  		<img src="../images/oasis_press_release_homepage-2.png" alt="Flower" width="137" height="137">
    		  	  		<div class="carousel-caption">
    		  	  			<h2>News</h2>
    		 	  			<h4>Oasis and Warehouse launch next generation websites, developed by Javelin Group.</h4>
    		 	  			<p>High street fashion brands Oasis and Warehouse are pleased to announce the launch of their innovative new digital platforms with the help of Accenture’s Javelin Group, Demandware and Retail Assist.</p>
    		  	  		</div>
    		  	  	</a>
    		  	</div>
    		  	
    		  	<div class="item">
    		  		<a href="">
    		  	  		<img src="../images/charles_tyrwhitt_home_news.png" alt="Flower" width="137" height="137">
    		  	  		<div class="carousel-caption">
    		  	  			<h2>News</h2>
    		 	  			<h4>Javelin Group develops and implements new website for Charles Tyrwhitt on the Demandware platform.</h4>
    		 	  			<p>British menswear retailer, Charles Tyrwhitt, has launched its new website with the help of Accenture’s Javelin Group and Demandware, which delivers a vastly improved customer experience and web performance.</p>
    		  	  		</div>
    		  	  	</a>
    		  	</div>
    		  	
    		  	<div class="item">
    		  		<a href="">
    		  	  		<img src="../images/tableau_partner_summit.png" alt="Flower" width="137" height="137">
    		  	  		<div class="carousel-caption">
    		  	  			<h2>News</h2>
    		 	  			<h4>Javelin Group wins EMEA Partner of the Year 2016 at the Tableau Partner Summit.</h4>
    		 	  			<p>Javelin Group was awarded EMEA Partner of the Year 2016 during Tableau's annual European, Middle-East and Africa (EMEA) Partner Summit in London.</p>
    		  	  		</div>
    		  	  	</a>
    		  	</div>
    		  	
    		  	<div class="item">
    		  		<a href="">
    		  	  		<img src="../images/wp_stock_optimisation.png" alt="Flower" width="137" height="137">
    		  	  		<div class="carousel-caption">
    		  	  			<h2>News</h2>
    		 	  			<h4>New White Paper: Optimising stock.</h4>
    		 	  			<p>This white paper examines the challenges that omni-channel retailing has created for stock control and explores the five key areas that a retailer needs to address to optimise inventory levels.</p>
    		  	  		</div>
    		  	  	</a>
    		  	</div>
    		  	
    		  	<div class="item">
    		  		<a href="">
    		  	  		<img src="../images/wp_venuescore_ireland-1.png" alt="Flower" width="137" height="137">
    		  	  		<div class="carousel-caption">
    		  	  			<h2>News</h2>
    		 	  			<h4>New Executive Summary: VENUESCORE Ireland 2016</h4>
    		 	  			<p>Following the success of VENUESCORE UK, which was first created over a decade ago, Javelin Group has now launched VENUESCORE Ireland 2016 in response to a resurging interest in the Irish retail property market.</p>
    		  	  		</div>
    		  	  	</a>
    		  	</div>
    		  
    		</div><!--end carousel-inner-->

    		<a class="left carousel-control" href="#myCarousel2" role="button" data-slide="prev">
    			<img src="../images/prev2.png">
    		</a>
    		<a class="right carousel-control" href="#myCarousel2" role="button" data-slide="next">
    		  <img src="../images/next2.png">
    		</a>
  		</div><!--end myCarousel2-->
  	</div><!--end container-->
  </div><!--end container-fluid page-header-->
  <hr>

  	<div class="container">
        <div class="row">
  			<div class="cols col-xs-12 col-sm-3">
  				<a href="">
  					<div class="homepage-tri-pods__pod__inner">
  						<h2>About</h2>
  						<p>Learn about our approach, our<br> team, and our retail expertise.</p>
  					</div>
  					<div style="background-image:url('../images/ties.jpg');" class="homepage-tri-pods__pod__img"></div>
  				</a>
  			</div>
  			<div class="cols col-xs-12 col-sm-3">
  				<a href="">
  					<div class="homepage-tri-pods__pod__inner">
  						<h2>Services</h2>
  						<p>Read about our retail consulting and systems integration services.</p>
  					</div>
  					<div style="background-image:url('../images/shopping_centre.jpg');" class="homepage-tri-pods__pod__img"></div>
  				</a>
  			</div>
  			<div class="cols col-xs-12 col-sm-3">
  				<a href="">
  					<div class="homepage-tri-pods__pod__inner">
  						<h2>Blog</h2>
  						<p>Stay informed about retail industry issues with our retail transformation blog.</p>
  					</div>
  					<div style="background-image:url('../images/keyboard.jpg');" class="homepage-tri-pods__pod__img"></div>
  				</a>
  			</div>
  			<div class="cols col-xs-12 col-sm-3">
  				<a href="">
  					<div class="homepage-tri-pods__pod__inner">
  						<h2>Accenture Strategy</h2>
  						<p>Accenture Strategy expands its digital capabilities in the retail industry, with the acquisition of Javelin Group.</p>
  					</div>
  					<div style="background-image:url('../images/Bontq_2961_Accenture-box_v1-2.jpg');" class="homepage-tri-pods__pod__img"></div>
  				</a>
  			</div>
  		</div><!--end row-->
  	</div><!--end container-->

   <!---->
</section><!--end section-->