	<!-- Footer -->
	<footer class="container-fluid footer text-left">
	  	<div class="container">
	  		<div class="row">
	  			<div class="container">
	  				<ul class="nav navbar-nav social navbar-light bg-faded">
	  					<li><a class="linkedin" href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
	  					<li><a class="twitter" href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
	  					<li><a class="youtube" href=""><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
	  				</ul>
	  			</div>
	  		</div>
	  		
	  		<div class="row">
	  				<div class="col-lg-2 col-md-2 col-sm-2 col_footer menu_top">
	  				<ul>
	  					<li class="menu-item">
	  						<a class="dropdown-toggle nav_footer" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="#">About</a>
				  			<ul class="sub-menu">
				  			  	<li><a href="#">Company</a></li>
				  			  	<li><a href="#">Management</a></li>
				  			  	<li><a href="#">Events</a></li>
				  			  	<li><a href="#">Press Releases</a></li>
				  			  	<li><a href="#">In the News</a></li> 
				  			</ul>
	  					</li>
	  				</ul>
	  				</div>
	  				<div class="col-lg-2 col-md-2 col-sm-2 col_footer menu_top">
	  				<ul>
	  					<li class="menu-item">
	  						<a class="dropdown-toggle nav_footer" data-toggle="dropdown" role="button" href="#" aria-haspopup="true" aria-expanded="false">Services</a>
							<ul class="sub-menu">
				  	  		  	<li><a href="#">Overview</a></li>
				  	  		  	<li><a href="#">Retail Strategy</a></li>
				  	  		  	<li><a href="#">Omni-Channel Retail</a></li>
				  	  		  	<li><a href="#">Operations</a></li> 
				  	  		  	<li><a href="#">Locations & Analytics</a></li> 
				  	  		  	<li><a href="#">Technology Consulting</a></li> 
				  	  		  	<li><a href="#">Due Diligence</a></li> 
				  	  		  	<li><a href="#">Systems Integration</a></li>  
				  	  		</ul>
	  					</li>
	  					</ul>
	  				</div>
	  				<div class="col-lg-2 col-md-2 col-sm-2 col_footer menu_top">
	  				<ul>
	  					<li class="menu-item">
	  						<a class="dropdown-toggle nav_footer" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="#">Clients</a>
				  			<ul class="sub-menu">
				  			  	<li><a href="#">Client List</a></li>
				  	  			<li><a href="#">Case Studies</a></li> 
				  			</ul>
	  					</li>
	  				</ul>
	  				</div>
	  				<div class="col-lg-2 col-md-2 col-sm-2 col_footer menu_top">
	  				<ul>
	  					<li class="menu-item">
	  						<a class="dropdown-toggle nav_footer" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="#">Insights</a>
				  			<ul class="sub-menu">
				  			  	<li><a href="#">White Papers</a></li>
				  	  		  	<li><a href="#">Features</a></li>
				  			</ul>
	  					</li>
	  				</ul>
	  				</div>
	  				<div class="col-lg-2 col-md-2 col-sm-2 col_footer">
	  				<ul>
	  					<li class="menu-item">
	  						<a class="dropdown-toggle nav_footer" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="#">Contact</a>
				  			<ul class="sub-menu-contact">
				  			  	<li><a href="#">United Kingdom</a></li>
				  			  	<li><a href="tel:+44 (0)20 7961 3200">+44 (0)20 7961 3200</a></li>
				  			  	<li><a href="#">France</a></li>
				  			  	<li><a href="tel:+33 (0)1 53 23 55 55">+33 (0)1 53 23 55 55</a></li>
				  			  	<li><a href="#">Bulgaria</a></li>
				  			  	<li><a href="tel:+359 (0)2 905 9477">+359 (0)2 905 9477</a></li> 
				  			</ul>
	  					</li>
	  				</ul>
	  				</div>
	  				<div class="col-lg-2 col-md-2 col-sm-2 col_footer">
	  				<ul>
	  					<li class="menu-item">
	  						<a class="dropdown-toggle nav_footer" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="#">Information</a>
				  			<ul class="sub-menu">
				  			  	<li><a href="#">T&C</a></li>
				  			  	<li><a href="#">Privacy</a></li>
				  			  	<li><a href="#">Site Map</a></li>
				  			  	<li><a href="#">Cookies</a></li>
				  			</ul>
	  					</li>
	  				</ul>
	  				</div>
	  		</div><!--row-->
	  		<div class="footer-bottom row">
        		<div class="container">
            		<p class="pull-left copyright">© 2017 Accenture. All Rights Reserved.</p>
        		</div>
    		</div><!--end footer-bottom-->
	  	</div>
	</footer>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.js"></script>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
 		$(function(){
 			$(window).scroll(function() { 
  				var top = $(document).scrollTop();
  				if (top > 30){
  					$('.main-header').addClass('fixed')
  				}else {
  					$('.main-header').removeClass('fixed')
  				}
 			});
		});
		$(function(){
 			$(window).scroll(function() { 
  				var top = $(document).scrollTop();
  				if (top > 70){
  					$('.fixed-tab').addClass('fixed_top')
  				}else {
  					$('.fixed-tab').removeClass('fixed_top')
  				}
 			});
		});
		$(document).ready(function(){
    		$(".search").click(function(){
    		    $(".navbar-fixed-top").hide();
    		    $("#search1").show();
    		});
		});
		$(document).ready(function(){
    		$(".colse_search").click(function(){
    		    $(".navbar-fixed-top").show();
    		    $("#search1").hide();
    		});
		});
	</script>
</body>
</html>